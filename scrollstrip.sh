#!/bin/sh

test $# -eq 9 || exit 1

D=300
IW=$(echo "11*72 * $D/72" | bc)
IH=$(echo "8.5*72 * $D/72" | bc)
TW=$(echo "$IW/4" | bc)
TH=$(echo "$IH/2" | bc)

SA=$(echo "(850 - (1100/4 * 1100/425)) / 2" | bc)
MW=$(echo "$IW / 40" | bc)
MH=$(echo "$IH / 40" | bc)
WMW=$(echo "$IW * 38/40" | bc)
WMH=$(echo "$IH * 38/40" | bc)

imageName() { test -n "$1" && echo "$1.png" || echo; }
orBlank() { test -e "$1" && echo "$1" || echo blank.png; }
I1="$(orBlank "$(imageName $1)")"
I2="$(orBlank "$(imageName $2)")"
I3="$(orBlank "$(imageName $3)")"
I4="$(orBlank "$(imageName $4)")"
I5="$(orBlank "$(imageName $5)")"
I6="$(orBlank "$(imageName $6)")"
I7="$(orBlank "$(imageName $7)")"
I8="$(orBlank "$(imageName $8)")"

convert -size 850x1100 canvas:transparent blank.png

# Trim horizontal sides of each page a bit, so that
# we can fit four on one paper.
convert "$I1" -shave ${SA}x0 temp1.png
convert "$I2" -shave ${SA}x0 temp2.png
convert "$I3" -shave ${SA}x0 temp3.png
convert "$I4" -shave ${SA}x0 temp4.png
convert "$I5" -shave ${SA}x0 temp5.png
convert "$I6" -shave ${SA}x0 temp6.png
convert "$I7" -shave ${SA}x0 temp7.png
convert "$I8" -shave ${SA}x0 temp8.png

montage -tile 4x2 -geometry ${TW}x${TH}+0+0 \
	temp4.png temp3.png temp2.png temp1.png \
	temp8.png temp7.png temp6.png temp5.png temp100.png

convert -geometry ${WMW}x${WMH} \
	-bordercolor white -border ${MW}x${MH} temp100.png \
	-density $D "$9"

rm blank.png temp100.png \
	temp1.png temp2.png temp3.png temp4.png \
	temp5.png temp6.png temp7.png temp8.png
