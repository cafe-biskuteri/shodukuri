#!/bin/sh

add() { echo "$1 + $2" | bc; }

LAST_PAGE=1
while test -e $LAST_PAGE.png
do
	LAST_PAGE=$(add $LAST_PAGE 1)
done

PAGE=0
LARGER_PAGE=1
TIME_ESTIMATE=0
while test $PAGE -lt $LAST_PAGE
do
	START=$(date +%s)

	../quadpage.sh \
		$(seq $PAGE $(add $PAGE 7)) \
		${LARGER_PAGE}f.pdf \
		${LARGER_PAGE}b.pdf
	PAGE=$(add $PAGE 8)
	LARGER_PAGE=$(add $LARGER_PAGE 1)

	ELAPSED=$(add $(date +%s) -$START)
	TIME_ESTIMATE=$(printf "%s + %s * (%s - %s)\n" \
		$TIME_ESTIMATE 0.5 $ELAPSED $TIME_ESTIMATE | bc)
	REMAINING_ESTIMATE=$(printf "(%s - %s) / 8 * %s\n" \
		$LAST_PAGE $PAGE $TIME_ESTIMATE | bc)
	printf "Page %s/%s Seconds remaining: %s\n" \
		$PAGE $LAST_PAGE $REMAINING_ESTIMATE
done

FILES=""
for I in $(seq 1 $LARGER_PAGE)
do
	test -e ${I}f.pdf && FILES="$FILES ${I}f.pdf"
	test -e ${I}b.pdf && FILES="$FILES ${I}b.pdf"
done
pdfunite $FILES completed.pdf

