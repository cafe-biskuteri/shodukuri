/* copyright

Shodukuri - Elementary Chinese/Japanese vertical text typesetter
Written in 2024 by Usawashi <usawashi16@yahoo.co.jp>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.

copyright */

import javax.swing.JComponent;
import javax.swing.JFrame;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileReader;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import javax.imageio.ImageIO;

class
Shodukuri extends JComponent
implements KeyListener {

	private String
	text;

	private List<List<String>>
	pages;

	private int
	firstPageIndex,
	currentPageIndex;

	private BufferedImage
	page;

	private Image
	scaledPage;

	private int
	lineCount;

	private List<Font>
	fonts;

	private int
	rowGapRatio;

	private Color
	borderColour,
	separatorColour,
	textColour;

//	---%-@-%---

	public void
	setText(String text)
	{
		this.text = text;

		int rowCount = computeRowCount();
		pages = toPages(toLines(text, rowCount), lineCount);
		currentPageIndex = firstPageIndex;

		paintPage();
	}

	public BufferedImage
	getPage(int index)
	{
		if (index < firstPageIndex) return null;
		if (index >= firstPageIndex + pages.size()) return null;

		BufferedImage page = new BufferedImage(
			this.page.getWidth(), this.page.getHeight(),
			this.page.getType());
		paintPage(page, index);

		return page;
	}

	public int
	getLastPageIndex()
	{
		return firstPageIndex + pages.size() - 1;
	}

//	 -	-%-	 -

	public void
	paintPage()
	{
		paintPage(this.page, currentPageIndex);
	}

	public void
	paintPage(BufferedImage page, int index)
	{
		Graphics g = page.getGraphics();
		int w = page.getWidth(), h = page.getHeight();

		applyRenderingHints(g);
		clear(g, w, h);

		int e = computeExtra(g, h);
		g.setColor(borderColour);
		paintSurroundingBorder(g, w, h, e);
		g.setColor(separatorColour);
		paintLineSeparators(g, w, h, e);

		g.setColor(textColour);
		paintText(g, w, h, index);
		paintPageNumber(g, w, h, e, index);

		g.dispose();
	}

	protected void
	applyRenderingHints(Graphics g)
	{
		if (!(g instanceof Graphics2D)) return;
		
		((Graphics2D)g).setRenderingHint(
			RenderingHints.KEY_TEXT_ANTIALIASING,
			RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
	}

	protected void
	clear(Graphics g, int w, int h)
	{
		if (!(g instanceof Graphics2D)) return;

		((Graphics2D)g).setBackground(new Color(0, 0, 0, 0));
		g.clearRect(0, 0, w, h);
	}

	protected void
	paintSurroundingBorder(Graphics g, int w, int h, int extra)
	{
		int xs = w * 2/20, xe = w * 18/20;
		int ys = h * 2/20 - extra/2;
		int ye = h * 18/20 + extra;

		int t = 4 * w/850;
		g.fillRect(xs - t, ys - t, (xe + t) - (xs - t), t);
		g.fillRect(xs - t, ye + 0, (xe + t) - (xs - t), t);
		g.fillRect(xs - t, ys, t, ye - ys);
		g.fillRect(xe + 0, ys, t, ye - ys);
	}

	protected void
	paintLineSeparators(Graphics g, int w, int h, int extra)
	{
		int xs = w * 2/20, xe = w * 18/20;
		int ys = h * 2/20 - extra/2;
		int ye = h * 18/20 + extra;

		int t = 1 * w/850;
		for (int i = 1; i < lineCount; ++i)
		{
			int x = xs + (xe - xs) * i/lineCount;
			g.fillRect(x - t/2, ys, t, ye - ys);
		}
	}

	protected void
	paintText(Graphics g, int w, int h, int index)
	{
		int xs = w * 2/20, xe = w * 18/20;
		int ys = h * 2/20, ye = h * 18/20;

		List<String> page = pages.get(index - firstPageIndex);

		FontMetrics fm = g.getFontMetrics(fonts.get(0));
		int rowHeight = fm.getAscent() * (100 + rowGapRatio) /100;
		
		int lineIndex = 1;
		for (String line: page)
		{
			for (int o = 0; o < line.length(); ++o)
			{
				char c = sub(line.charAt(o));

				Font font = fontFor(c);
				if (font == null) font = fonts.get(0);
				g.setFont(font);
				fm = g.getFontMetrics();
				int ascent = fm.getAscent();
				int charWidth = fm.charWidth(c);

				int rowIndex = 1 + o;
				int baseline =
					ys + rowIndex * rowHeight
					- rowHeight * 1/8;

				int lineCentre =
					xe
					+ (xe - xs) * 1/(lineCount * 2)
					- (xe - xs) * lineIndex/lineCount;
				int x = lineCentre - (charWidth / 2);
	
				g.drawString("" + c, x, baseline);
			}
			++lineIndex;
		}
	}

	protected void
	paintPageNumber(Graphics g, int w, int h, int extra, int index)
	{
		int xs = w * 2/20, xe = w * 18/20;
		int ye = h * 18/20 + extra;

		String string = toChineseNumber(index);
		if (string.isEmpty()) return;

		Font font = fontFor('〇');
		if (font == null) return;
		font = font.deriveFont(font.getSize() * 0.8f);

		g.setFont(font);
		FontMetrics fm = g.getFontMetrics();
		int labelWidth = fm.stringWidth(string);
		int labelHeight = fm.getAscent();

		int lineCentre = xs + (xe - xs) / 2;
		int baseline = ye + (extra / 2) + labelHeight;
		int x = lineCentre - (labelWidth / 2);
		g.drawString(string, x, baseline);
	}

	protected Font
	fontFor(char c)
	{
		for (Font font: fonts) if (font.canDisplay(c)) return font;
		return null;
	}

	protected int
	computeExtra(Graphics g, int h)
	{
		FontMetrics fm = g.getFontMetrics(fonts.get(0));
		int rowHeight = fm.getAscent() * (100 + rowGapRatio) /100;
		int lineHeight = computeRowCount() * rowHeight;
		int unusedHeight = (h * 18/20) - (h * 2/20) - lineHeight;
		int neededExtra = rowHeight - unusedHeight;
		return neededExtra;
	}

	private int
	computeRowCount()
	{
		int h = page.getHeight();
		int ys = h * 2/20, ye = h * 18/20;

		FontMetrics fm = getFontMetrics(fonts.get(0));
		int rowHeight = fm.getAscent() * (100 + rowGapRatio) /100;

		return (ye - ys) / rowHeight;
	}

	protected void
	paintComponent(Graphics g)
	{
		boolean yetScaled = scaledPage == null;
		int sw = yetScaled ? 0 : scaledPage.getWidth(null);
		int sh = yetScaled ? 0 : scaledPage.getHeight(null);
		if (sw != getWidth())
			scaledPage = page.getScaledInstance(
				getWidth(), getHeight(),
				BufferedImage.SCALE_SMOOTH);

		g.drawImage(scaledPage, 0, 0, this);
	}

	public void
	keyPressed(KeyEvent eK)
	{
		int code = eK.getKeyCode();

		int pagesToTheRight = currentPageIndex - firstPageIndex;
		int pagesToTheLeft = pages.size() - pagesToTheRight -1;

		if (code == KeyEvent.VK_LEFT && pagesToTheLeft > 0)
		{
			++currentPageIndex;
			paintPage();
			scaledPage = null;
			repaint();
		}
		else if (code == KeyEvent.VK_RIGHT && pagesToTheRight > 0)
		{
			--currentPageIndex;
			paintPage();
			scaledPage = null;
			repaint();
		}
	}

	public void
	keyTyped(KeyEvent eK) { }

	public void
	keyReleased(KeyEvent eK) { }

//	 -	-%-	 -

	private static char
	sub(char c)
	{
		// https://en.wiktionary.org/wiki/
		// Appendix:Unicode/CJK_Compatibility_Forms
		if (c == '「') return '\uFE41';
		if (c == '」') return '\uFE42';
		if (c == '『') return '\uFE43';
		if (c == '』') return '\uFE44';
		if (c == '（') return '\uFE35';
		if (c == '）') return '\uFE36';
		if (c == '［') return '\uFE47';
		if (c == '］') return '\uFE48';
		if (c == '【') return '\uFE3B';
		if (c == '】') return '\uFE3C';
		if (c == '―') return '\uFF5C';
		if (c == 'ー') return '\uFE31';
		if (c == '–') return '\uFE32';
		if (c == '…') return '\uFE19';
		if (c == '。') return '\uFE12';
		if (c == '、') return '\uFE11';
		return c;
	}

	private static boolean
	canBeInMargin(char c)
	{
		if (c == '」') return true;
		if (c == '』') return true;
		if (c == '）') return true;
		if (c == '］') return true;
		if (c == '】') return true;
		if (c == '。') return true;
		if (c == '、') return true;
		return false;
	}

	private static List<String>
	toLines(String text, int lineLength)
	{
		List<String> lines = new ArrayList<>();
		StringBuilder line = new StringBuilder();
		for (int o = 0; o < text.length(); ++o)
		{
			char c = text.charAt(o);

			boolean newline = c == '\n';
			boolean lineFull = line.length() >= lineLength;
			boolean push = newline ^ lineFull;

			boolean marginal = canBeInMargin(c);
			
			if (push && !marginal)
			{
				lines.add(line.toString());
				line.delete(0, line.length());
			}

			if (!newline) line.append(c);
		}
		lines.add(line.toString());

		return lines;
	}

	private static List<List<String>>
	toPages(List<String> lines, int pageLength)
	{
		List<List<String>> pages = new ArrayList<>();
		List<String> page = new ArrayList<>();
		for (String line: lines)
		{
			page.add(line);
			if (page.size() == pageLength)
			{
				pages.add(page);
				page = new ArrayList<>();
			}
		}
		pages.add(page);
		return pages;
	}

	private static String
	toChineseNumber(int number)
	{
		String table = "〇一二三四五六七八九";
		StringBuilder b = new StringBuilder(4);
		int thousands = (number / 1000) % 10;
		int hundreds = (number / 100) % 10;
		int tens = (number / 10) % 10;
		int ones = (number / 1) % 10;
		if (number >= 1000) b.append(table.charAt(thousands));
		if (number >= 100) b.append(table.charAt(hundreds));
		if (number >= 10) b.append(table.charAt(tens));
		if (number >= 1) b.append(table.charAt(ones));
		return b.toString();
	}

//	---%-@-%---

	public static class
	Profile {

		int
		pageWidth, pageHeight;

		int
		lineCount;

		List<Font>
		fonts;

		String
		text;

		int
		firstPageIndex;

		int
		rowGapRatio;

		Color
		borderColour,
		separatorColour,
		textColour;

	}

//	---%-@-%---

	public static void
	main(String... args)
	throws IOException
	{
		Profile profile; try
		{
			profile = readProfileFromStdin();
			validateProfile(profile);
		}
		catch (IllegalArgumentException eIa)
		{
			System.err.println(eIa.getMessage());
			return;
		}

		Shodukuri instance = new Shodukuri(profile);

		JFrame mainframe = new JFrame("書作り");
		mainframe
			.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		mainframe.setLocationByPlatform(true);

		mainframe.add(instance);
		mainframe.pack();
		mainframe.setVisible(true);

		if (args.length >= 1 && args[0].equals("save"))
		{
			new File("output").mkdir();
			int li = instance.getLastPageIndex();
			for (int i = profile.firstPageIndex; i <= li; ++i)
			{
				imageToFile(
					instance.getPage(i),
					"output/" + i + ".png");
			}
		}
	}

//	 -	-%-	 -

	private static Profile
	readProfileFromStdin()
	throws IllegalArgumentException, IOException
	{
		Profile returnee = new Profile();
		returnee.fonts = new ArrayList<>();
		returnee.lineCount = 16;
		returnee.firstPageIndex = 1;
		returnee.rowGapRatio = -1;

		returnee.borderColour = new Color(205, 190, 220);
		returnee.separatorColour = new Color(165, 150, 180);
		returnee.textColour = new Color(60, 60, 60);	

		BufferedReader r = new BufferedReader(
			new InputStreamReader(System.in));
		String line; while ((line = r.readLine()) != null) try
		{
			String[] fields = line.split(":");
			if (fields[0].startsWith("#")) continue;
			else if (fields[0].equals("size"))
			{
				returnee.pageWidth = Integer.parseInt(fields[1]);
				returnee.pageHeight = Integer.parseInt(fields[2]);
			}
			else if (fields[0].equals("lines"))
			{
				returnee.lineCount = Integer.parseInt(fields[1]);
			}
			else if (fields[0].equals("first page index"))
			{
				returnee.firstPageIndex = Integer.parseInt(fields[1]);
			}
			else if (fields[0].equals("text file"))
			{
				FileReader r2 = new FileReader(fields[1]);
				StringBuilder b = new StringBuilder();
				int c; while ((c = r2.read()) != -1) b.append((char)c);
				returnee.text = b.toString();
				b.setLength(0);
			}
			else if (fields[0].equals("font"))
			{
				String name = fields[1];
				int size = Integer.parseInt(fields[2]);
				Font font = new Font(name, Font.PLAIN, size);
				returnee.fonts.add(font);
			}
			else if (fields[0].equals("row gap ratio"))
			{
				returnee.rowGapRatio = Integer.parseInt(fields[1]);
			}
			else if (fields[0].equals("border colour"))
			{
				returnee.borderColour = parseColour(fields);
			}
			else if (fields[0].equals("separator colour"))
			{
				returnee.separatorColour = parseColour(fields);
			}
			else if (fields[0].equals("text colour"))
			{
				returnee.textColour = parseColour(fields);
			}
		}
		catch (IndexOutOfBoundsException eOob)
		{
			String msg = "A line doesn't have enough fields..!";
			throw new IllegalArgumentException(msg);
		}
		catch (NumberFormatException eNf)
		{
			String msg = "Non-number was given..!";
			msg += "(" + eNf.getMessage() + ")";
			throw new IllegalArgumentException(msg, eNf);
		}

		return returnee;
	}

	private static Color
	parseColour(String[] fields)
	{
		return new Color(
			Integer.parseInt(fields[1]),
			Integer.parseInt(fields[2]),
			Integer.parseInt(fields[3]),
			fields.length >= 5
				? Integer.parseInt(fields[4]) : 255);
	}

	public static void
	validateProfile(Profile profile)
	throws IllegalArgumentException
	{
		if (profile.pageWidth == 0)
			throw new IllegalArgumentException
				("Page width is zero (or wasn't given).");
		if (profile.pageHeight == 0)
			throw new IllegalArgumentException
				("Page height is zero (or wasn't given).");

		if (profile.fonts.isEmpty())
			throw new IllegalArgumentException
				("No font specified.");

		if (profile.text == null)
			throw new IllegalArgumentException
				("Text file path not specified.");

		if (profile.rowGapRatio < 0)
			throw new IllegalArgumentException
				("Row gap ratio not specified.");

		if (profile.borderColour == null)
			throw new IllegalArgumentException
				("Border colour not specified.");
		if (profile.separatorColour == null)
			throw new IllegalArgumentException
				("Separator colour not specified.");
		if (profile.textColour == null)
			throw new IllegalArgumentException
				("Text colour not specified.");
	}

	private static void
	imageToFile(BufferedImage image, String filepath)
	throws IOException
	{
		ImageIO.write(image, "png", new File(filepath));
	}

//	---%-@-%---

	Shodukuri(Profile profile)
	{
		page = new BufferedImage(
			profile.pageWidth, profile.pageHeight,
			BufferedImage.TYPE_INT_ARGB);
		fonts = profile.fonts;
		lineCount = profile.lineCount;
		firstPageIndex = profile.firstPageIndex;
		rowGapRatio = profile.rowGapRatio;
		borderColour = profile.borderColour;
		separatorColour = profile.separatorColour;
		textColour = profile.textColour;

		currentPageIndex = firstPageIndex;

		setText(profile.text);
		autoPreferredSize();
		addKeyListener(this);
		setFocusable(true);
	}

	private void
	autoPreferredSize()
	{
		Dimension pgsz = 
			new Dimension(page.getWidth(), page.getHeight());
		Dimension scrsz =
			Toolkit.getDefaultToolkit().getScreenSize();
		Dimension psz = new Dimension(pgsz.width, pgsz.height);
		while (psz.height >= scrsz.height)
			psz.height = psz.height * 2/3;
		psz.width = psz.width * psz.height/pgsz.height;
		setPreferredSize(psz);
	}

}