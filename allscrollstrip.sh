#!/bin/sh

add() { echo "$1 + $2" | bc; }

LAST_PAGE=1
while test -e $LAST_PAGE.png
do
	LAST_PAGE=$(add $LAST_PAGE 1)
done

PAGE=1
LARGER_PAGE=1
TIME_ESTIMATE=0
FILES=""
while test $PAGE -lt $LAST_PAGE
do
	START=$(date +%s)

	../scrollstrip.sh \
		$(seq $PAGE $(add $PAGE 7)) \
		${LARGER_PAGE}.pdf
	FILES="$FILES ${LARGER_PAGE}.pdf"
	PAGE=$(add $PAGE 8)
	LARGER_PAGE=$(add $LARGER_PAGE 1)

	ELAPSED=$(add $(date +%s) -$START)
	TIME_ESTIMATE=$(printf "%s + %s * (%s - %s)\n" \
		$TIME_ESTIMATE 0.5 $ELAPSED $TIME_ESTIMATE | bc)
	REMAINING_ESTIMATE=$(printf "(%s - %s) / 8 * %s\n" \
		$LAST_PAGE $PAGE $TIME_ESTIMATE | bc)
	printf "Page %s/%s Seconds remaining: %s\n" \
		$PAGE $LAST_PAGE $REMAINING_ESTIMATE
done

../scrollstrip.sh 0 _ _ _ _ _ _ _ 0.pdf

pdfunite $FILES completed.pdf
