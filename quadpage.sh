#!/bin/sh

test $# -eq 10 || exit 1

D=300
IW=$(echo "8.5*72 * $D/72" | bc)
IH=$(echo "11*72 * $D/72" | bc)

imageName() { test -n "$1" && echo "$1.png" || echo; }
orBlank() { test -e "$1" && echo "$1" || echo blank.png; }
I1="$(orBlank "$(imageName $1)")"
I2="$(orBlank "$(imageName $2)")"
I3="$(orBlank "$(imageName $3)")"
I4="$(orBlank "$(imageName $4)")"
I5="$(orBlank "$(imageName $5)")"
I6="$(orBlank "$(imageName $6)")"
I7="$(orBlank "$(imageName $7)")"
I8="$(orBlank "$(imageName $8)")"

convert -size 1x1 canvas:transparent blank.png

TW=$(echo "$IW/2" | bc)
TH=$(echo "$IH/2" | bc)
TWS=$(echo "$TW/40" | bc)
TS=${TW}x${TH}!

set -x

convert -background transparent \
	\( -resize $TS -repage -${TWS} "$I1" \) \
	-layers flatten temp1.png
# Multi-layering is the only reliable way to get ImageMagick
# to do this sort of operation for one image. Everything else
# just absurdly doesn't work.

convert -background transparent \
	\( -resize $TS -repage +${TWS} "$I4" \) \
	-layers flatten temp2.png

convert -background transparent \
	\( -resize $TS  -repage -${TWS} "$I5" \) \
	-layers flatten temp3.png

convert -background transparent \
	\( -resize $TS  -repage +${TWS} "$I8" \) \
	-layers flatten temp4.png

montage -tile 2x2 -density $D -geometry +0+0 \
	temp1.png temp2.png temp3.png temp4.png "$9"
# The default geometry is pretty unusable, override it with auto.


convert -background transparent \
	\( -resize $TS -repage -${TWS} "$I3" \) \
	-layers flatten temp5.png

convert -background transparent \
	\( -resize $TS -repage +${TWS} "$I2" \) \
	-layers flatten temp6.png

convert -background transparent \
	\( -resize $TS  -repage -${TWS} "$I7" \) \
	-layers flatten temp7.png

convert -background transparent \
	\( -resize $TS  -repage +${TWS} "$I6" \) \
	-layers flatten temp8.png

montage -tile 2x2 -density $D -geometry +0+0 \
	temp5.png temp6.png temp7.png temp8.png "${10}"

set +x

rm blank.png \
	temp1.png temp2.png temp3.png temp4.png \
	temp5.png temp6.png temp7.png temp8.png
